# Tic Tac Toe Project

**This Tic Tac TOE Project is a Java Desktop Application.

## ⚡️ Requirements

- A solid interface with [JavaFX](https://openjfx.io/) (`v11`)
- A superset of [Java](https://www.java.com/en/) (`v11` or newer)


## 🔥 Stack

- [Maven](https://maven.apache.org/), a software project management and comprehension tool.

## 💻 How to use

After cloning the repository ,run the following command:

```shell
$ mvn install
$ mvn javafx:run
```

Now you need try my application!

## 🕺 Contribute

**Want to hack on `Tic Tac Toe  Project`? Follow the next instructions:**

1. Fork this repository to your own git account and then clone it to your local device
2. Install dependencies as we explained at this documentation.
4. Send a pull request 🙌

Good luck, developer! 🚀
