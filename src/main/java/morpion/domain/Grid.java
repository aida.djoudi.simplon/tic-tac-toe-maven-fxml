package morpion.domain;

public class Grid {

    private int row;
    private int column;
    private char[][] matrix;
    //creation de la grid dans constructure est donner a tout les case une valeur vide

    public Grid(int row, int column) {
        this.row = row;
        this.column = column;
        this.matrix = new char[row][column];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                matrix[i][j] = ' ';
            }
        }
    }

    //les geters et les setters comme les attribut sont tous privé

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public void setMatrix(char[][] matrix) {
        this.matrix = matrix;
    }

    //affichage de la grid

    public void displayGrid() {
        System.out.println();
        int k = 1;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {

                System.out.print("|" + k + ":" + matrix[i][j]);
                k++;
            }
            System.out.println("|");
        }
    }
    //entrer la valeur de la case soit X ou O avec vérification si input est valable ou pas

    /**
     *
     * @param playerChoiceCase
     * @param currentPlayer
     */

    public void setGame(int playerChoiceCase, Player currentPlayer) {

                int i = (playerChoiceCase - 1) / 3;
                int j = (playerChoiceCase - 1) % 3;

                matrix[i][j] = currentPlayer.jeton;


    }
    public boolean checkCase (int playerChoiceCase){
        int i = (playerChoiceCase - 1) / 3;
        int j = (playerChoiceCase - 1) % 3;
        if( (matrix[i][j]== 'X') || (matrix[i][j]== 'O')) {
            System.out.println("c'est déja rempli");
            return false;
        }
        else{

           return true;

        }

    }
    public boolean checkWinner(Player currentPlayer) {
        boolean testWinner=false;
        //vérification de la ligne

        for(int i=0 ; i<row; i++) {
            int j = 0;
            if ((matrix[i][j] == matrix[i][j + 1]) &&
                    (matrix[i][j + 1] == matrix[i][j + 2]) &&
                    (matrix[i][j + 2] == currentPlayer.jeton)) {
                System.out.println("bravo" +" "+ currentPlayer.name + " "+"tu as gané ");
                testWinner = true;


            }
        }
        //vérification de la ligne
        for(int j=0 ; j< column; j++) {
            int i = 0;
            if ((matrix[i][j] == matrix[i + 1][j]) &&
                    (matrix[i + 1][j] == matrix[i + 2][j]) &&
                    (matrix[i + 2][j] == currentPlayer.jeton)) {
                System.out.println("bravo" +" "+ currentPlayer.name +" "+"tu as gané ");
                testWinner = true;
            }
        }
        //vérification de la diagonale

        if ((matrix[0][0] == matrix[1][1]) &&
                (matrix[1][1] == matrix[2][2]) &&
                (matrix[2][2] == currentPlayer.jeton)) {
            System.out.println("bravo" +" "+ currentPlayer.name +" "+ "tu as gané ");
            testWinner = true;
        }
        //vérification de la diagonale
        if ((matrix[2][0] == matrix[1][1]) &&
                (matrix[1][1] == matrix[0][2]) &&
                (matrix[0][2] == currentPlayer.jeton)) {
            System.out.println("bravo" +" "+ currentPlayer.name +" "+ "tu as gané");
            testWinner = true;
        }
        return  testWinner;
    }
    public boolean checkFullGrid() {
        boolean full= true;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if((matrix[i][j] == ' ')) {
                    full =false;
                }
            }

        }
        return full ;

    }
}









