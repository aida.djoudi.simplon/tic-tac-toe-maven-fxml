package morpion.presentation.controllers;

import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import morpion.domain.Grid;
import morpion.domain.Player;

public class Controller_screen {
    //créer les joueur
    private Player player1 = new Player("", 'X');
    private Player player2 = new Player("", 'O');
    private Player currentPlayer ;
    private Label labelNamePlayer = new Label(" ");
    @FXML
    private TextField namePlayer1;
    @FXML
    private TextField namePlayer2;
    @FXML
    private Label quiJoue;
    // Grille
    private Grid myGrid = new Grid(3,3);
    //label de resultat
    @FXML
    private Label result0fGame;
    @FXML
    private GridPane grille;
    @FXML
    private HBox hboxPlayer1;
    @FXML
    private HBox hboxPlayer2;
    @FXML
    private Button quit;
    @FXML
    private Button rejoue;
    @FXML
    private Button start;
    @FXML
    private AnchorPane scene2;


    public void clickStartButton(){
        start.setVisible(false);
        grille.setGridLinesVisible(true);
        grille.setCursor(Cursor.HAND);
        hboxPlayer1.setVisible(false);
        hboxPlayer2.setVisible(false);
        // récupère les noms des joueurs
        quiJoue.setVisible(true);
        result0fGame.setVisible(false);
        quit.setVisible(false);
        rejoue.setVisible(false);
        currentPlayer =player1;
        //avoir le nom des players rentrer dans textfeild
        player1.name=namePlayer1.getText();
        player2.name=namePlayer2.getText();
        System.out.println( player1.name);
        System.out.println( player1.name);


        if(currentPlayer.name !=""){
            quiJoue.setText("le joueur "+" "+"***"+currentPlayer.name+"***"+" "+"joue");
        }
        else{
            quiJoue.setText("joueur1 commence!");
        }

    }

    public void clickGridButton(MouseEvent e) {
        Node source = (Node) e.getTarget();
        Integer colIndex = GridPane.getColumnIndex(source);
        Integer rowIndex = GridPane.getRowIndex(source);
        System.out.println("index" + GridPane.getRowIndex(source));
        System.out.println("colomn" + GridPane.getColumnIndex(source));
        int idI = rowIndex * 3 + colIndex;
        System.out.println("idI" + idI);
        int idII = idI + 1;

        //avoir le nom des players rentrer dans textfeild

        if(myGrid.checkCase(idII)){

            myGrid.setGame(idII , currentPlayer);
            System.out.println(currentPlayer.name);

            /**
             * myGrid.checkWinner(currentPlayer) cad
             * que on'a un gagnant par ce que cette fonction
             * retourne true pour gagnant et
             * false si il n'ya pas
             */
            if( myGrid.checkWinner(currentPlayer)){

                if(currentPlayer.name !="") {
                    result0fGame.setText("Bravo" + " " + "***"+ currentPlayer.name+"***"+" "+"vous avez gagné");


                }
                else{
                    if(currentPlayer==player1){
                        System.out.println(player1.name);
                        result0fGame.setText("Bravo" + " " + "***"+"Joueur1"+"***"+" "+"vous avez gagné");

                    }
                    if(currentPlayer==player2){
                        System.out.println(player1.name);
                        result0fGame.setText("Bravo" + " " + "***"+ "Joueur2"+"***"+" "+"vous avez gagné");

                    }

                }


                grille.setDisable(true);

            }

            if( myGrid.checkFullGrid()){
                result0fGame.setText("égalité");
                grille.setDisable(true);


            }
            result0fGame.setVisible(true);
            rejoue.setVisible(true);
            quit.setVisible(true);
            switchPlayer(idI);



        }
        else {
            Alert a = new Alert(Alert.AlertType.NONE);
            a.setAlertType(Alert.AlertType.WARNING);
            a.setContentText("la case est déja rempli");
            a.show();

        }


    }
    public void clickExitButton(){
        System.exit(0);
        System.out.println("exit");
    }
    public void clickRestartButton(){

        grille.setDisable(false);

        start.setVisible(true);
        //  grille.setDisable(true);
        quiJoue.setVisible(false);
        quit.setVisible(false);
        rejoue.setVisible(false);
        grille.setGridLinesVisible(false);
        hboxPlayer1.setVisible(true);
        hboxPlayer2.setVisible(true);
        result0fGame.setText(" ");

        quiJoue.setText("Le joueur 1 commence !");
        //   myGrid.setGrid();
        // réinitialisation de myGrid
        myGrid = new Grid(3,3);
        for(int i=0 ; i<9; i++){
            ImageView im = (ImageView) grille.getChildren().get(i);
            im.setImage(null);
        }

        System.out.println("restard");

    }
    public void switchPlayer(int idI){
        if (currentPlayer == player1) {
            Image imageOk = new Image("file:src/main/resources/images/x.png");
            System.out.println("idI"+idI);
            ImageView imageView = (ImageView) grille.getChildren().get(idI);
            imageView.setImage(imageOk);
            currentPlayer= player2;
        } else {

            Image imageOk = new Image("file:src/main/resources/images/o.png");
            System.out.println("idI"+idI);
            ImageView imageView = (ImageView) grille.getChildren().get(idI);
            imageView.setImage(imageOk);
            currentPlayer = player1;
        }
        if(currentPlayer.name !=""){
            quiJoue.setText("le joueur "+" "+"***"+currentPlayer.name+"***"+" "+"joue");

        }
        else{
            if(currentPlayer==player1){
                System.out.println(player1.name);
                quiJoue.setText("le joueur "+" "+"***"+"Joueur1"+"***"+" "+"joue");
            }
            if(currentPlayer==player2){
                System.out.println(player1.name);
                quiJoue.setText("le joueur "+" "+"***"+"Joueur2"+"***"+" "+"joue");
            }
        }
    }
}
